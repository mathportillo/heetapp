package com.hackathon.heet.database;

import java.lang.reflect.Field;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class UserSession {
	
	public String fbID;
	public String name;
	public String username;
	public String access_token;
	
	private static String FILE_REF = "heet_user";
	
	private UserSession() {
		
	}
	
	public boolean isLogged(){
		if (fbID == null)
			return false;
		return true;
	}

	private static class SessionHolder {
		public static final UserSession instance = new UserSession();
	}
 
	public static UserSession getInstance() {
		return SessionHolder.instance;
	}

	public void save(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
		Gson gson = new Gson();
		String json = gson.toJson(this);

		Editor prefsEditor = prefs.edit();
		prefsEditor.putString(FILE_REF, json);
		prefsEditor.commit(); 
	}

	public void logout(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
		Editor prefsEditor = prefs.edit();
		prefsEditor.remove(FILE_REF);
		prefsEditor.commit();
		fbID = "";
		name = "";
	}

	public static UserSession loadSession(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
		Gson gson = new Gson();
		String json = prefs.getString(FILE_REF, "");
		
		if (!json.equals(""))
			try {
				UserSession aux = gson.fromJson(json, UserSession.class);
				Class<?> clazz = aux.getClass();
				
				for (Field field : clazz.getFields()) {
					Object value;
					try {
						value = field.get(aux);
						field.set(getInstance(), value);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e){
				e.printStackTrace();
			}
			
		return getInstance();
	}

	
}