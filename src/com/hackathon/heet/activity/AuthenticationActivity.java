package com.hackathon.heet.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.hackathon.heet.R;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.hackathon.heet.api.AuthenticationAPI;
import com.hackathon.heet.api.AuthenticationAPI.OnLogged;
import com.hackathon.heet.authentication.SigninFragment;
import com.hackathon.heet.database.UserSession;

public class AuthenticationActivity extends SherlockFragmentActivity implements OnLogged {

	public static final int FRAGMENT_SIGNIN = 1;
	
	private UserSession mSession;
	public static Session mFBSession;

	//private View mFragment;

	private ProgressDialog ringProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.authentication_activity);
	
		//mFragment = (View) findViewById(R.id.authentication_activity_fragment_content);
		
		mSession = UserSession.getInstance();
		
		if (savedInstanceState != null) {
			return;
		}

		goToFragment(FRAGMENT_SIGNIN, null, false);
	}

	public void goToFragment(int fragmentCode, Bundle args, boolean back ) {
		Fragment fragment;

		switch (fragmentCode) {
		case FRAGMENT_SIGNIN:
			fragment = new SigninFragment();
			break;

		default:
			throw new Error("Fragment with code " + fragmentCode + " not found");
		}

		fragment.setArguments(args);

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		if(back) {
			transaction.addToBackStack(fragment.getClass().getName());
		}
		transaction.replace(R.id.authentication_activity_fragment_content, fragment)
		.commit();

	}	
	
	public void showProgress(String message) {
		if(ringProgressDialog != null && ringProgressDialog.isShowing()) {
			ringProgressDialog.setMessage(message);
		} else {
			ringProgressDialog = ProgressDialog.show(this, getString(R.string.loading), message, true);
			ringProgressDialog.setCancelable(true);
			ringProgressDialog.setIcon(R.drawable.ic_drawer_logo);
		}
	}
		
	public void showMainFragment() {
		if( ringProgressDialog != null )
			ringProgressDialog.dismiss();
	}
	
	public void goToMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
    	startActivity(intent);
	}
	
	public void loginWithFacebook() {
		Session s = new Session(this);
		Session.OpenRequest request = new Session.OpenRequest(this);
		request.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO); // <-- this is the important line
		request.setCallback(new Session.StatusCallback() {
	    	
		      // callback when session changes state
		      @Override
		      public void call(Session session, SessionState state, Exception exception) {
		    	  
				if (exception instanceof FacebookOperationCanceledException) {
					showMainFragment();
				}
		    	  
		        if (session.isOpened()) {
		        	mSession.access_token = session.getAccessToken();
		        	mFBSession = session;
		          // make request to the /me API
		          Request.newMeRequest(session, new Request.GraphUserCallback() {

		            // callback after Graph API response with user object
		            @Override
		            public void onCompleted(GraphUser user, Response response) {
		              if (user != null) {
		            	  Log.i("JSON", "" + user.getInnerJSONObject());
		            	  	mSession.username = user.getUsername();
		            	  	mSession.fbID = user.getId();
		          			mSession.name = user.getName();
		          			mSession.save(getApplicationContext());
		          			login();
		               }
		            }
		          }).executeAsync();
		        }
		      }
		    });
		
		s.openForRead(request);
		Session.setActiveSession(s);
		
		/*// start Facebook Login
	    Session.openActiveSession(this, true, new Session.StatusCallback() {
	    	
	      // callback when session changes state
	      @Override
	      public void call(Session session, SessionState state, Exception exception) {
	        if (session.isOpened()) {
	        	mSession.access_token = session.getAccessToken();
	          // make request to the /me API
	          Request.newMeRequest(session, new Request.GraphUserCallback() {

	            // callback after Graph API response with user object
	            @Override
	            public void onCompleted(GraphUser user, Response response) {
	              if (user != null) {
	            	  Log.i("JSON", "" + user.getInnerJSONObject());
	            	  	mSession.username = user.getUsername();
	            	  	mSession.fbID = user.getId();
	          			mSession.name = user.getName();
	          			mSession.save(getApplicationContext());
	          			login();
	               }
	            }
	          }).executeAsync();
	        }
	      }
	    });*/
	}
	
	private void login() {
		//showProgress("Logging in...");
		AuthenticationAPI api = new AuthenticationAPI(getApplicationContext());
		api.login(mSession.fbID, mSession.name, mSession.username, this);
	}
	
	@Override
	  public void onActivityResult(int requestCode, int resultCode, Intent data) {
	      super.onActivityResult(requestCode, resultCode, data);
	      
	      Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	  }

	@Override
	public void onLogged(boolean success, String errorMessage, int errorCode) {
		showMainFragment();	
		if(success) {			
			goToMainActivity();
		} else {
			Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_LONG).show();
		}
	}
	
}
