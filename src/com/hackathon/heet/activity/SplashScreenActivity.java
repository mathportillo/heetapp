package com.hackathon.heet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

import com.actionbarsherlock.app.SherlockActivity;
import com.hackathon.heet.R;
import com.hackathon.heet.database.UserSession;
 
public class SplashScreenActivity extends SherlockActivity{
    
    private Thread mSplashThread; 
    private boolean mblnClicou = false;
    private UserSession mUserSession;
 
    /** Evento chamado quando a activity � executada pela primeira vez */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        
        mUserSession = UserSession.loadSession(getApplicationContext());
    
        //thread para mostrar uma tela de Splash
        mSplashThread = new Thread() {
            @Override
            public void run() {
             try {
                    synchronized(this){
                     //Espera por 3 segundos ou sai quando
                     //o usu�rio tocar na tela
                        wait(3000);
                        mblnClicou = true;
                    }
                }
                catch(InterruptedException ex){                    
                }
                 
                if (mblnClicou){
                 //fechar a tela de Splash
                    finish();
                    
                    if(mUserSession.fbID != null){
                    	if(mUserSession.fbID.equals("")) {
                    		goToAuthentication();
   	                 	} else {
   	                 		goToApp();
   	                 	}
                    } else {
                    	goToAuthentication();
                    }
                     
                 
                }
            }
        };
         
        mSplashThread.start();
    }
    
    private void goToApp() {
    	//Carrega a Activity Principal
        Intent i = new Intent();
        i.setClass(SplashScreenActivity.this, MainActivity.class);
        startActivity(i);
    }
    
    private void goToAuthentication() {
    	//Carrega a Activity de Login
       Intent i = new Intent();
       i.setClass(SplashScreenActivity.this, AuthenticationActivity.class);
       startActivity(i);
    }
     
    @Override
    public void onPause()
    {
        super.onPause();
         
        //garante que quando o usu�rio clicar no bot�o
        //"Voltar" o sistema deve finalizar a thread
        mSplashThread.interrupt();
    }
     
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            //o m�todo abaixo est� relacionado a thread de splash
         synchronized(mSplashThread){
          mblnClicou = true;
           
             //o m�todo abaixo finaliza o comando wait
             //mesmo que ele n�o tenha terminado sua espera
                mSplashThread.notifyAll();
            }            
        }
        return true;
    }
 
}