package com.hackathon.heet.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.hackathon.heet.R;
import com.hackathon.heet.api.BumpAPI;
import com.hackathon.heet.api.BumpAPI.OnInfoSent;
import com.hackathon.heet.database.UserSession;
import com.hackathon.heet.modules.FriendsFragment;
import com.hackathon.heet.modules.GPSTracker;
import com.hackathon.heet.modules.HomeFragment;
import com.hackathon.heet.modules.MenuListAdapter;
import com.hackathon.heet.modules.ShakeDetectActivity;
import com.hackathon.heet.modules.ShakeDetectActivityListener;
import com.hackathon.heet.modules.WebFragment;

public class MainActivity extends SherlockFragmentActivity implements OnInfoSent {

	// Declare Variables
	DrawerLayout mDrawerLayout;
	ListView mDrawerList;
	ActionBarDrawerToggle mDrawerToggle;
	MenuListAdapter mMenuAdapter;
	String[] title;
	String[] subtitle;
	int[] icon;
	Fragment fragment1 = new HomeFragment();
	Fragment fragment2 = new FriendsFragment();
	Fragment fragment3 = new HomeFragment();
	private CharSequence mDrawerTitle;
	public CharSequence mTitle;
	
	private double latitude;
	private double longitude;
	private long time;
	
	public String idReal;
	
	ShakeDetectActivity shakeDetectActivity;
	
	public ShakeDetectActivity getShakeDetectActivity() {
		return shakeDetectActivity;
	}

	Vibrator vibrator;
	
	public Vibrator getVibrator() {
		return vibrator;
	}

	private UserSession mSession;
	
	private ProgressDialog ringProgressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from drawer_main.xml
		setContentView(R.layout.drawer_main);
		
		shakeDetectActivity = new ShakeDetectActivity(this);
		shakeDetectActivity.addListener(new ShakeDetectActivityListener() {
			@Override
			public void shakeDetected() {
				MainActivity.this.triggerShakeDetected();
			}
		});
		vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
		
		mSession = UserSession.getInstance();

		// Get the Title
		mTitle = mDrawerTitle = getTitle();

		// Generate title
		title = new String[] { getString(R.string.home_fragment), getString(R.string.past_fragment),
				getString(R.string.logout) };

		// Generate icon
		icon = new int[] { R.drawable.home, R.drawable.users,
				R.drawable.logout };

		// Locate DrawerLayout in drawer_main.xml
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// Locate ListView in drawer_main.xml
		mDrawerList = (ListView) findViewById(R.id.listview_drawer);

		// Set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		// Pass string arrays to MenuListAdapter
		mMenuAdapter = new MenuListAdapter(MainActivity.this, title,
				icon);

		// Set the MenuListAdapter to the ListView
		mDrawerList.setAdapter(mMenuAdapter);

		// Capture listview menu item click
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setLogo(R.drawable.ic_drawer_logo);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				super.onDrawerClosed(view);
			}

			public void onDrawerOpened(View drawerView) {
				// Set the title on the action when drawer open
				getSupportActionBar().setTitle(mDrawerTitle);
				super.onDrawerOpened(drawerView);
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
		}

		return super.onOptionsItemSelected(item);
	}

	// ListView click listener in the navigation drawer
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if(position < 2)
				selectItem(position);
			else if (position == 2)
				showLogoutDialog();
		}
	}

	private void selectItem(int position) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		// Locate Position
		switch (position) {
		case 0:
			ft.replace(R.id.content_frame, fragment1);
			break;
		case 1:
			ft.replace(R.id.content_frame, fragment2);
			break;
		case 2:
			ft.replace(R.id.content_frame, fragment3);
			break;
		}
		ft.commit();
		mDrawerList.setItemChecked(position, true);

		// Get the title followed by the position
		setTitle(title[position]);
		// Close drawer
		mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	public void goToFragment(Fragment fragment, Bundle args, boolean back) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

			ft.replace(R.id.content_frame, fragment);
			fragment.setArguments(args);
			if(back) {
				ft.addToBackStack(fragment.getClass().getName());
		    }
		ft.commit();
		
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}
	
	public void showProgress(String message) {
		if(ringProgressDialog != null && ringProgressDialog.isShowing()) {
			ringProgressDialog.setMessage(message);
		} else {
			ringProgressDialog = ProgressDialog.show(this, getString(R.string.loading), message, true);
			ringProgressDialog.setCancelable(true);
			ringProgressDialog.setIcon(R.drawable.ic_drawer_logo);
		}
	}
		
	public void showMainFragment() {
		if( ringProgressDialog != null )
			ringProgressDialog.dismiss();
	}
	
	protected void showLogoutDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder
				.setTitle(getString(R.string.logout))
                .setMessage(getString(R.string.logout_message))
                .setCancelable(false);
		
                alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                	public void onClick(DialogInterface dialog, int id) {
                		dialog.dismiss();
                	}
                });
                alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                	public void onClick(DialogInterface dialog, int id) {
                		mSession.logout(MainActivity.this);
                		if (AuthenticationActivity.mFBSession != null) AuthenticationActivity.mFBSession.closeAndClearTokenInformation();
            			goToAuthentication();                		
                	}
                });
		        alertDialogBuilder.setIcon(R.drawable.ic_drawer_logo);
		        
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
	}
	
	private void goToAuthentication() {
		Intent intent = new Intent(this, AuthenticationActivity.class);
		startActivity(intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		shakeDetectActivity.onResume();
	}

	@Override
	protected void onPause() {
		shakeDetectActivity.onPause();
		super.onPause();
	}

	public void triggerShakeDetected() {
		time = System.currentTimeMillis();
		vibrator.vibrate(500);
		try {
		    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
		    r.play();
		    getLocation();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		//Toast.makeText(getApplicationContext(), String.valueOf(time), ShakeDetectActivity.IGNORE_EVENTS_AFTER_SHAKE).show();
	}
	
	public void getLocation() {
		final GPSTracker gps = new GPSTracker(getApplicationContext());
		
				// check if GPS enabled     
		        if(gps.canGetLocation()){
		             
		            latitude = gps.getLatitude();
		            longitude = gps.getLongitude();
		            sendInfo();

		        }else{
		            // can't get location
		            // GPS or Network is not enabled
		            // Ask user to enable GPS/network in settings
		            gps.showSettingsAlert();
		        }
	}
	
	private void sendInfo() {
		showProgress(getString(R.string.loading_connection));
		BumpAPI api = new BumpAPI(getApplicationContext());
		api.sendInfo(latitude, longitude, mSession.fbID, time, this);
	}

	@Override
	public void onInfoSent(boolean success, String username, boolean successQuery, String errorMessage,
			int errorCode) {
		showMainFragment();	
		if(success) {	
			if(successQuery)
				openWebFacebookProfile(username);
				//openFacebookProfile(username);
			else
				Toast.makeText(getApplicationContext(), getString(R.string.wait_friend), Toast.LENGTH_LONG).show();
			//getRealID(username);
			
		} else {
			Toast.makeText(getApplicationContext(), getString(R.string.wait_friend), Toast.LENGTH_LONG).show();
		}
	}
	
	/*private void getRealID(String username) {
		JSONParser json = new JSONParser();
		json.execute("https://graph.facebook.com/" + username + "?access_token=" + session.access_token);
		json.execute("http://graph.facebook.com/" + username);
		
		BumpAPI api = new BumpAPI();
		api.getRealId(username, this);
	}*/
	
	public static Intent getOpenFacebookIntent(Context context, String fbID) {

	    try {
	    	//return new Intent(Intent.ACTION_VIEW,
	          //      Uri.parse("https://www.facebook.com/app_scoped_user_id/" + fbID)); //catches and opens a url to the desired page
	        context.getPackageManager()
	                .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
	        return new Intent(Intent.ACTION_VIEW,
	                Uri.parse("fb://profile/" + fbID)); //Trys to make intent with FB's URI*/
	    } catch (Exception e) {
	        return new Intent(Intent.ACTION_VIEW,
	                Uri.parse("https://www.facebook.com/app_scoped_user_id/" + fbID)); //catches and opens a url to the desired page
	    }
	}
	
	/*public void openFacebookProfile(String fbID){
		Intent facebookIntent = getOpenFacebookIntent(this, fbID);
		startActivity(facebookIntent);
	}*/
	
	private void openWebFacebookProfile(String fbID) {
		Bundle args = new Bundle();
		args.putString("fbID", fbID);
		
		goToFragment(new WebFragment(), args, true);
	}
	
}
