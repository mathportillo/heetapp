package com.hackathon.heet.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class JsonREST {
	
	// FLAGS:
	//
	// usage:
	// if it's a INTERNET OR TIMEOUT EXCEPTION
	// if((errorCode & (JsonREST.EXCEPTION_INTERNET | JsonREST.EXCEPTION_TIMEOUT)) != 0)
	//
	public static final int EXCEPTION_NONE = 0x00;
	public static final int EXCEPTION_TIMEOUT = 0x01;
	public static final int EXCEPTION_INTERNET = 0x02;
	public static final int EXCEPTION_JSON = 0x04;
	public static final int EXCEPTION_ILLEGAL = 0x08;

	public static final String EXCEPTION_TIMEOUT_MESSAGE = "Falha de conexão.";
	public static final String EXCEPTION_INTERNET_MESSAGE = "Falha de conexão.";
	public static final String EXCEPTION_JSON_MESSAGE = "Erro interno.";
	
	
	private static final int HTTP_GET = 1;
	private static final int HTTP_POST = 2;
	private static final int HTTP_POST_JSON = 3;
	private static final int HTTP_PUT = 4;
	private static final int HTTP_PUT_JSON = 5;
	private static final int HTTP_DELETE = 6;

	private JsonHandler lastHandler;
	private Map <String, String> lastHeaders;
	private StringEntity lastStringEntity;
	private int lastHttp;
	private int lastCodeReq;
	private String lastUrl;
	private String mContentTypeGet;
	
	
	public static String getAbsoluteUrl(String relativeUrl, Map<String, String> params) {
		return formatUrlWithParams(Configs.BASE_URL + relativeUrl, params);
	}
	

	public void get(String url, JsonHandler handler, String contentType, Map<String, String> headers, int codeReq) {
		mContentTypeGet = contentType;
		
		AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, HTTP_GET);
		asyncHttp.execute(url);
	}
	
	public void get(String url, JsonHandler handler, int codeReq) {
		AsyncHttp asyncHttp = new AsyncHttp(handler, codeReq, HTTP_GET);
		asyncHttp.execute(url);
	}

	public void put(String url, Map<String, String> body, JsonHandler handler, Map<String, String> headers, int codeReq) throws UnsupportedEncodingException {
		StringEntity stringEntity = new StringEntity("");
		if(body != null)
			stringEntity = mapToStringEntity(body);
		stringEntity.setContentType("application/x-www-form-urlencoded");

		AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, stringEntity, HTTP_PUT);
		asyncHttp.execute(url);
	}
	
	public void put(String url, JSONObject json, JsonHandler handler, Map<String, String> headers, int codeReq) throws UnsupportedEncodingException {
		StringEntity stringEntity = new StringEntity("");
		if(json != null)
			stringEntity = new StringEntity(json.toString(), "utf-8");

		AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, stringEntity, HTTP_PUT_JSON);
		asyncHttp.execute(url);
	}

	public void post(String url, Map<String, String> body, JsonHandler handler, Map<String, String> headers, int codeReq) {
		try {
			StringEntity stringEntity = new StringEntity("");
			if(body != null)
				stringEntity = mapToStringEntity(body);
			stringEntity.setContentType("application/x-www-form-urlencoded");

			AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, stringEntity, HTTP_POST);
			asyncHttp.execute(url);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public void post(String url, JSONObject json, JsonHandler handler, Map<String, String> headers, int codeReq){
		try {
			StringEntity stringEntity = new StringEntity("");
			if(json != null)
				stringEntity = new StringEntity(json.toString(), "utf-8");
			
			AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, stringEntity, HTTP_POST_JSON);
			asyncHttp.execute(url);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void delete(String url, JsonHandler handler, Map<String, String> headers, int codeReq) throws UnsupportedEncodingException {
		AsyncHttp asyncHttp = new AsyncHttp(handler, headers, codeReq, HTTP_DELETE);
		asyncHttp.execute(url);
	}
	
	private StringEntity mapToStringEntity(Map<String, String> map) throws UnsupportedEncodingException {
		StringBuilder stringBuilder = new StringBuilder();
		for(Map.Entry<String, String> entry : map.entrySet()) {
			if(entry.getValue() != null) {
				stringBuilder.append(entry.getKey()).append('=').append(URLEncoder.encode(entry.getValue(), "UTF-8")).append('&');
			}
		}
		
		// remove last & stupid... but... =/
		if(stringBuilder.length() > 0) {
			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		}
	      
		return new StringEntity(stringBuilder.toString());
	}

	public void tryAgain(){
		AsyncHttp asyncHttp;
		asyncHttp = new AsyncHttp(lastHandler, lastHeaders, lastCodeReq, lastStringEntity, lastHttp);
		asyncHttp.execute(lastUrl);
	}

	public static String formatUrlWithParams(String url, Map<String, String> params) {
		String urlRes = url;
		
		if(params == null || params.size() == 0) {
			return urlRes;
		}
		
		if(!urlRes.endsWith("?"))
			urlRes += "?";
		
		List<NameValuePair> paramsNameValuePair = new LinkedList<NameValuePair>();
		for(Map.Entry<String, String> entry : params.entrySet()) {
			paramsNameValuePair.add(new BasicNameValuePair(entry.getKey(), entry.getValue()) );
		}
		urlRes += URLEncodedUtils.format(paramsNameValuePair, "utf-8");
		
		return urlRes;
	}



	public class AsyncHttp extends AsyncTask<String, Integer, String>{
		private int HTTP_REQUEST;
		private String url;
		private int codeReq;
		private JsonHandler handler;
		private StringEntity stringEntity;
		private HttpResponse response;
		private HttpClient httpclient;
		private Map<String, String> headers;
		private int errorCode = EXCEPTION_NONE;
		private int httpStatusCode;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		public AsyncHttp(JsonHandler handler, int codeReq, int http) {
			this.headers = new HashMap<String, String>();
			this.handler = handler;
			this.codeReq = codeReq;
			this.HTTP_REQUEST = http;
		}
		
		public AsyncHttp(JsonHandler handler, Map<String, String> headers, int codeReq, int http) {
			this.handler = handler;
			this.headers = headers;
			this.codeReq = codeReq;
			this.HTTP_REQUEST = http;
		}

		public AsyncHttp(JsonHandler handler, Map<String, String> headers, int codeReq, StringEntity stringEntity, int http) {
			this.handler = handler;
			this.headers = headers;
			this.codeReq = codeReq;
			this.stringEntity = stringEntity;
			this.HTTP_REQUEST = http;
		}

		@Override
		protected String doInBackground(String... uri){
			JsonREST.this.lastCodeReq = this.codeReq;
			JsonREST.this.lastHandler = this.handler;
			JsonREST.this.lastHttp = this.HTTP_REQUEST;
			JsonREST.this.lastStringEntity = this.stringEntity;
			JsonREST.this.lastHeaders = this.headers;
			JsonREST.this.lastUrl = uri[0];

			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
			HttpConnectionParams.setSoTimeout(httpParameters, 20000);
			httpclient = new DefaultHttpClient(httpParameters);
			String responseString = "";
			this.url = uri[0];
			
			Log.d(Configs.LOG_TAG, "HTTP REQUEST URL: " + url);
			
			try {
				executeRequest();
				if(response == null) {
					errorCode = EXCEPTION_INTERNET;
					return null;
				}
				StatusLine statusLine = response.getStatusLine();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				httpStatusCode = statusLine.getStatusCode();
				
			} catch (ConnectTimeoutException e) {
				errorCode = EXCEPTION_TIMEOUT;
			} catch (SocketTimeoutException e) {
				errorCode = EXCEPTION_TIMEOUT;
			} catch (IOException e) {
				errorCode = EXCEPTION_INTERNET;
			}
			return responseString;
		}

		protected void executeRequest() throws ClientProtocolException, IOException{
			switch (HTTP_REQUEST) {

			case HTTP_GET:
				HttpGet httpGet = new HttpGet(this.url);
				for(Map.Entry<String, String> entry : this.headers.entrySet()) {
					httpGet.setHeader(entry.getKey(), entry.getValue());
				}
				
				if(!mContentTypeGet.equals("")) 
					httpGet.setHeader("Content-type", mContentTypeGet + "; charset=utf-8");

				this.response = httpclient.execute(httpGet);
				break;

			case HTTP_POST:
				HttpPost httpPost = new HttpPost(this.url);
				for(Map.Entry<String, String> entry : this.headers.entrySet()) {
					httpPost.setHeader(entry.getKey(), entry.getValue());
				}

				httpPost.setHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
				httpPost.setEntity(this.stringEntity);
				this.response = httpclient.execute(httpPost);
				break;
				
				//POST PARA JSON
			case HTTP_POST_JSON:
				HttpPost httpPostJson = new HttpPost(this.url);
				
				/*if(this.headers.entrySet() != null) {
					for(Map.Entry<String, String> entry : this.headers.entrySet()) {
						httpPostJson.setHeader(entry.getKey(), entry.getValue());
					}
				}*/

				httpPostJson.setHeader("Content-type", "application/json");
				httpPostJson.setEntity(this.stringEntity);
				this.response = httpclient.execute(httpPostJson);
				break;

			case HTTP_PUT:
				HttpPut httpPut = new HttpPut(this.url);
				for(Map.Entry<String, String> entry : this.headers.entrySet()) {
					httpPut.setHeader(entry.getKey(), entry.getValue());
				}

				httpPut.setHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
				httpPut.setEntity(this.stringEntity);
				this.response = httpclient.execute(httpPut);
				break;
				
			case HTTP_PUT_JSON:
				HttpPut httpPutJson = new HttpPut(this.url);
				for(Map.Entry<String, String> entry : this.headers.entrySet()) {
					httpPutJson.setHeader(entry.getKey(), entry.getValue());
				}

				httpPutJson.setHeader("Content-type", "application/json");
				httpPutJson.setEntity(this.stringEntity);
				this.response = httpclient.execute(httpPutJson);
				break;

			case HTTP_DELETE:
				HttpDelete httpDelete = new HttpDelete(this.url);
				for(Map.Entry<String, String> entry : this.headers.entrySet()) {
					httpDelete.setHeader(entry.getKey(), entry.getValue());
				}

				this.response = httpclient.execute(httpDelete);
				break;

			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				Log.d(JsonREST.class.getName(), ""+result);
				
				if (errorCode != EXCEPTION_NONE)
					handler.onFailure(errorCode, httpStatusCode, codeReq);

				if (result.startsWith("{")){
					handler.onSuccess(EXCEPTION_NONE, httpStatusCode, new JSONObject(result), codeReq);
				}	else if (result.startsWith("[")) {
					handler.onSuccess(EXCEPTION_NONE, httpStatusCode, new JSONArray(result), codeReq);
				} else {
					handler.onFailure(EXCEPTION_JSON, httpStatusCode, codeReq);
				}
			} catch (JSONException e) {
				handler.onFailure(EXCEPTION_JSON, httpStatusCode, codeReq);
			}
		}
	}
	
	
	
	
	
	
	public interface JsonHandler {

		public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int code);
		
		public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int code);

		public void onFailure(int errorCode, int httpStatusCode, int code);
	}

}