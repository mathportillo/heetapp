package com.hackathon.heet.api;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.hackathon.heet.R;
import com.hackathon.heet.model.Friend;
import com.hackathon.heet.utils.JsonREST;
import com.hackathon.heet.utils.JsonREST.JsonHandler;

public class BumpAPI implements JsonHandler {

	private static final String URL_SEND  = "bump.php";
	private static final String URL_GET_PAST  = "past.php";
	
	private static final int CODE_SEND = 1;
	private static final int CODE_GET_PAST = 2;

	private OnInfoSent onInfoSent;
	private OnGotPast onGotPast;
	
	private Context mContext;
	
	public BumpAPI(Context context) {
		mContext = context;
	}

	public void sendInfo(double lat, double lon, String fbID, long time, OnInfoSent handler) {
		this.onInfoSent = handler;

		try {
			
			JSONObject infoObj = new JSONObject();
			infoObj.put("lat", lat);
			infoObj.put("lon", lon);
			infoObj.put("fbID", fbID);
			infoObj.put("time", time);
		
			JsonREST jsonREST = new JsonREST();
			jsonREST.post(
					JsonREST.getAbsoluteUrl(URL_SEND, null),
					infoObj,
					this,
					null,
					CODE_SEND
					);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void onInfoSent(JSONObject response, boolean success, int errorCode) {
		if (!success) {
			String errorMessage = mContext.getString(R.string.error_connection);
			onInfoSent.onInfoSent(false, null, false, errorMessage, errorCode);
			return;
		}

		if(!response.optBoolean("success", false)) {
			onInfoSent.onInfoSent(false, null, false, response.optString("errorMessage"), errorCode);
		} else {			
			
			String fbID = response.optString("fbID");
			boolean successquery = response.optBoolean("successquery");
			
			onInfoSent.onInfoSent(true, fbID, successquery, null, errorCode);		
		}
	}
	
	public void getPast(String fbID, OnGotPast handler) {
		this.onGotPast = handler;

		try {
			
			JSONObject infoObj = new JSONObject();
			infoObj.put("fbID", fbID);
		
			JsonREST jsonREST = new JsonREST();
			jsonREST.post(
					JsonREST.getAbsoluteUrl(URL_GET_PAST, null),
					infoObj,
					this,
					null,
					CODE_GET_PAST
					);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void onGotPast(JSONObject response, boolean success, int errorCode) {
		if (!success) {
			String errorMessage = mContext.getString(R.string.error_connection);
			onGotPast.onGotPast(false, null, errorMessage, errorCode);
			return;
		}

		if(!response.optBoolean("success", false)) {
			onGotPast.onGotPast(false, null, response.optString("errorMessage"), errorCode);
		} else {			
			try {
				List<Friend> friends = new ArrayList<Friend>();
				
				JSONArray friendsArrayJson = response.getJSONArray("connections");
				for(int i = 0; i < friendsArrayJson.length(); i++) {
					JSONObject contactJson = friendsArrayJson.getJSONObject(i);
					
					Friend friend = new Friend();
					friend.name = contactJson.optString("name");
					friend.fbID = contactJson.optString("fbID");
										
					friends.add(friend);
				}			
			
			onGotPast.onGotPast(true, friends, null, errorCode);	
			
			} catch (JSONException e) {
				onGotPast.onGotPast(false, null, JsonREST.EXCEPTION_JSON_MESSAGE, JsonREST.EXCEPTION_JSON);
			}
		}
	}
	
	@Override
	public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int codeReq) {
		switch (codeReq) {
		case CODE_SEND:
			onInfoSent(response, true, errorCode);
			break;
		case CODE_GET_PAST:
			onGotPast(response, true, errorCode);
			break;
			
		default:
			throw new Error(this.getClass().getName() + " with invalid code (" + codeReq + ")");
		}
	}
	
	@Override
	public void onFailure(int errorCode, int httpStatusCode, int codeReq) {
		switch (codeReq) {
		case CODE_SEND:
			onInfoSent(null, false, errorCode);
			break;
		case CODE_GET_PAST:
			onGotPast(null, false, errorCode);
			break;
		
		default:
			throw new Error(this.getClass().getName() + " with invalid code (" + codeReq + ")");
		}
	}
	

	@Override
	public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int codeReq) {
	}

	public interface OnInfoSent {
		void onInfoSent(boolean success, String fbID, boolean success_query,String errorMessage, int errorCode);
	}
	
	public interface OnGotPast {
		void onGotPast(boolean success, List<Friend> friends, String errorMessage, int errorCode);
	}
	
	
}