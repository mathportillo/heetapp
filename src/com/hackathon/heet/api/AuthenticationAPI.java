package com.hackathon.heet.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.hackathon.heet.R;
import com.hackathon.heet.utils.JsonREST;
import com.hackathon.heet.utils.JsonREST.JsonHandler;

public class AuthenticationAPI implements JsonHandler {

	private static final String URL_LOGIN  = "login.php";
	
	private static final int CODE_LOGIN = 1;

	private OnLogged onLogged;
	
	private Context mContext;
	
	public AuthenticationAPI(Context context) {
		mContext = context;
	}

	public void login(String fbID, String name, String username, OnLogged handler) {
		this.onLogged = handler;

		try {
			
			JSONObject loginObj = new JSONObject();
			loginObj.put("fbID", fbID);
			loginObj.put("name", name);
			loginObj.put("username", username);
		
			JsonREST jsonREST = new JsonREST();
			jsonREST.post(
					JsonREST.getAbsoluteUrl(URL_LOGIN, null),
					loginObj,
					this,
					null,
					CODE_LOGIN
					);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void onLogged(JSONObject response, boolean success, int errorCode) {
		if (!success) {
			String errorMessage = mContext.getString(R.string.error_connection);
			onLogged.onLogged(false, errorMessage, errorCode);
			return;
		}

		if(!response.optBoolean("success", false)) {
			onLogged.onLogged(false, response.optString("errorMessage"), errorCode);
		} else {			
			onLogged.onLogged(true, null, errorCode);		
		}
	}	
	
	@Override
	public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int codeReq) {
		switch (codeReq) {
		case CODE_LOGIN:
			onLogged(response, true, errorCode);
			break;
			
		default:
			throw new Error(this.getClass().getName() + " with invalid code (" + codeReq + ")");
		}
	}
	
	@Override
	public void onFailure(int errorCode, int httpStatusCode, int codeReq) {
		switch (codeReq) {
		case CODE_LOGIN:
			onLogged(null, false, errorCode);
			break;
		
		default:
			throw new Error(this.getClass().getName() + " with invalid code (" + codeReq + ")");
		}
	}
	

	@Override
	public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int codeReq) {
	}

	public interface OnLogged {
		void onLogged(boolean success, String errorMessage, int errorCode);
	}
	
}