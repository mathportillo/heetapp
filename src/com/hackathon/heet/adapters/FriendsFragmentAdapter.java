package com.hackathon.heet.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hackathon.heet.R;
import com.hackathon.heet.model.Friend;

public class FriendsFragmentAdapter extends BaseAdapter {

	private Context mContext;
	private List<Friend> mFriends;

	public FriendsFragmentAdapter(Context context) {
		mContext = context;
		mFriends = new ArrayList<Friend>();
	}
	
	public void setFriends(List<Friend> friends) {
		mFriends = friends;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (mFriends == null)
			mFriends = new ArrayList<Friend>();
		return mFriends.size();
	}

	@Override
	public Object getItem(int index) {
		return mFriends.get(index);
	}

	@Override
	public long getItemId(int index) {
		return index;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.friends_custom_list_item, parent, false);
		}
		
		Friend friend = mFriends.get(index);		

		TextView nameView = (TextView) view.findViewById(R.id.txtName);
		nameView.setText(mContext.getString(R.string.connected_with) + " " + friend.name);	
		
		ImageView thumbnail = (ImageView) view.findViewById(R.id.list_image);		
		ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
		
		if(friend.photo == null) {
			thumbnail.setVisibility(View.GONE);
			BitmapAsyncTask bitmapAsyncTask = new BitmapAsyncTask(thumbnail, friend.photo, progressBar);
			bitmapAsyncTask.execute("https://graph.facebook.com/"+friend.fbID+"/picture?type=large");
		} else {
			progressBar.setVisibility(View.GONE);
			thumbnail.setImageBitmap(friend.photo);
		}
		
		return view;
	}
	
	private class BitmapAsyncTask extends AsyncTask<String, String, Bitmap>
    {
		ImageView imageView;
		Bitmap photo;
		ProgressBar progressBar;
        
		public BitmapAsyncTask(ImageView imageView, Bitmap photo, ProgressBar progressBar) {
			this.imageView = imageView;
			this.photo = photo;
			this.progressBar = progressBar;
		}
		
        @Override
		protected Bitmap doInBackground(String... url){
            HttpURLConnection connection;
			try {
				connection = (HttpURLConnection) new URL(url[0]).openConnection();
	            InputStream input = connection.getInputStream();

	            return BitmapFactory.decodeStream(input);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}
        
        @Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if(result != null) {
				imageView.setImageBitmap(result);
				photo = result;
			}
			imageView.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.GONE);
		}
    }

}
