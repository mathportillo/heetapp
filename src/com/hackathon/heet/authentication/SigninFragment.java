package com.hackathon.heet.authentication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.hackathon.heet.R;

public class SigninFragment extends BaseAppFragment {
	
	private Button btnLogin;
	
	// Instance of Facebook Class
	 
	 //private static String APP_ID = "754043467969615"; // Replace with your App ID
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container, false);		
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	    btnLogin = (Button) getView().findViewById(R.id.btnLogin);
	    btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getAuthenticationActivity().showProgress(getAuthenticationActivity().getString(R.string.loading_login));
				getAuthenticationActivity().loginWithFacebook();
			}
		});
	    
	}
	
}
