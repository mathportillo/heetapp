package com.hackathon.heet.authentication;

import com.actionbarsherlock.app.SherlockFragment;
import com.hackathon.heet.activity.AuthenticationActivity;

abstract public class BaseAppFragment extends SherlockFragment {
    
	protected AuthenticationActivity getAuthenticationActivity() {
		return (AuthenticationActivity) getActivity();
	}
	
}
