package com.hackathon.heet.modules;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hackathon.heet.R;

public class WebFragment extends BaseAppFragment {

	private WebView mWebView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_web, container,
				false);

		mWebView = (WebView) rootView.findViewById(R.id.webview);

		return rootView;
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		String fbID = getArguments().getString("fbID");

		mWebView.setWebViewClient(new WebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.loadUrl("https://www.facebook.com/app_scoped_user_id/" + fbID);

		final ProgressDialog progressDialog = createLoadingDialog(getMainActivity().getString(R.string.loading_profile));
		progressDialog.show();
		mWebView.setWebViewClient(new MyWebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				if (progressDialog.isShowing() && progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		});
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return false;
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			super.onReceivedSslError(view, handler, error);

			// this will ignore the Ssl error and will go forward to your site
			handler.proceed();
		}
	}

	private ProgressDialog createLoadingDialog(String message) {
		ProgressDialog progressDialog = new ProgressDialog(getMainActivity());
		progressDialog.setTitle(getMainActivity().getString(R.string.loading));
		progressDialog.setMessage(message);
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.setIcon(R.drawable.ic_drawer_logo);
		return progressDialog;
	}
	
	@Override
    public void onResume() {
        super.onResume();
        
        getMainActivity().mTitle = getMainActivity().getString(R.string.profile_fragment);
        getMainActivity().getActionBar().setTitle(getMainActivity().getString(R.string.profile_fragment));
    }

}
