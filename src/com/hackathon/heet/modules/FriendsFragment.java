package com.hackathon.heet.modules;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.hackathon.heet.R;
import com.hackathon.heet.adapters.FriendsFragmentAdapter;
import com.hackathon.heet.api.BumpAPI;
import com.hackathon.heet.api.BumpAPI.OnGotPast;
import com.hackathon.heet.database.UserSession;
import com.hackathon.heet.model.Friend;

public class FriendsFragment extends BaseAppListFragment implements OnGotPast {
	public static boolean showDetails = false;
	private ListView mListView;

	FriendsFragmentAdapter mAdapter;
	MatrixCursor mMatrixCursor;
	List<Friend> mFriends;
	
	Context mContext;
	
	private UserSession mSession;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_friends, container, false);
				
		mSession = UserSession.getInstance();
		
		setHasOptionsMenu(true);
		
		mListView = (ListView) rootView.findViewById(android.R.id.list);
		mFriends = new ArrayList<Friend>();
		mAdapter = new FriendsFragmentAdapter(getActivity());
		
		getAllFriends();
		
		return rootView;
	}
	
	private void getAllFriends() {
		getMainActivity().showProgress(getMainActivity().getString(R.string.loading_friends));
		BumpAPI api = new BumpAPI(getMainActivity());
		api.getPast(mSession.fbID, this);
	}

	@Override
	public void onGotPast(boolean success, List<Friend> friends,
			String errorMessage, int errorCode) {
		if(success) {
			mFriends = friends;
			mAdapter.setFriends(mFriends);
    		mAdapter.notifyDataSetChanged();
    		mListView.setAdapter(mAdapter);
		} else {
			Toast.makeText(getMainActivity(), getMainActivity().getString(R.string.error_friends), Toast.LENGTH_LONG).show();
		}		
		getMainActivity().showMainFragment();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
//		super.onListItemClick(l, v, position, id);
		String fbID = mFriends.get(position).fbID;
		
		Bundle args = new Bundle();
		args.putString("fbID", fbID);

		getMainActivity().goToFragment(new WebFragment(), args, true);
	}
	
	@Override
    public void onResume() {
        super.onResume();
        
        getMainActivity().mTitle = getMainActivity().getString(R.string.past_fragment);
    }

}
