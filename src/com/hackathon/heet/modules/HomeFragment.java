package com.hackathon.heet.modules;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackathon.heet.R;

public class HomeFragment extends BaseAppFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home, container, false);		
		
		//Toast.makeText(getActivity(), "USER ID: " + session.fbID + " | NAME: " + session.name, Toast.LENGTH_LONG).show();
	
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);		
		
	}
	
	@Override
    public void onResume() {
        super.onResume();
        
        getMainActivity().mTitle = getMainActivity().getString(R.string.home_fragment);
    }

}
