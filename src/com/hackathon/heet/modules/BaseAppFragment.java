package com.hackathon.heet.modules;

import com.actionbarsherlock.app.SherlockFragment;
import com.hackathon.heet.activity.MainActivity;

abstract public class BaseAppFragment extends SherlockFragment {
    
	protected MainActivity getMainActivity() {
		return (MainActivity) getActivity();
	}
	
}
