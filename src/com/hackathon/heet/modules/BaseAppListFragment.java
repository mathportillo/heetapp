package com.hackathon.heet.modules;

import com.actionbarsherlock.app.SherlockListFragment;
import com.hackathon.heet.activity.MainActivity;

abstract public class BaseAppListFragment extends SherlockListFragment {
    
	protected MainActivity getMainActivity() {
		return (MainActivity) getActivity();
	}
	
}
